import axios from "axios"
import md5 from "crypto-js/md5.js"
const private_key = ""
const public_key = ""
const redirect_url = ""
const api = axios.create({
    // eslint-disable-next-line no-undef
    baseURL: 'https://vpos.infonet.com.py:8888/vpos/api/0.3/',
    timeout: 20000,
    headers: {
      "Content-Type": "application/json",
    },
})

export default {
    createCard({user,card}) {
        let token = md5(private_key + card.id + user.id + "request_new_card").toString()
        let operation = {
            "token" : token,
            "card_id" : card.id,
            "user_id" : user.id,
            "user_cell_phone" : user.phone,
            "user_mail" : user.email,
            "return_url" : redirect_url
        };
        let body = {
            public_key,
            operation,
            test_client:true
        };
        return api.post('/cards/new',body);
    }
}
